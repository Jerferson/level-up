# LevelUp

Este projeto foi gerado com [Angular CLI](https://github.com/angular/angular-cli) versão 10.2.1.

## Development server

Execute `npm install` para instalação dos pacotes.
Execute `ng serve` para executar a aplicação em ambiente de desenvolvimento. Navegue para `http://localhost:4200/`. O aplicativo será recarregado automaticamente se você alterar qualquer um dos arquivos.

## Build

Execute `ng build --prod` para executar o biuld do projeto para produção.

## Deploy app

Após executar o build do projeto é necessário pegar os arquivos da pasta `/dist/` que foram gerados.
Para publicar o app em um servidor Apache é necessário fazer a configuração para que o servidor retorne o arquivo `index.html` quando não encontrar nada.
Essa configuração é importante para que sua aplicação consiga tratar as rotas da forma coreta podendo executar reload na tela e a mesma carregar normalmente.
Opção - 1
Necessário adicionar a seguinte configuração no aquivo `httpd.conf` do Apache:

```
RewriteEngine On
RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -f [OR]
RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -d
RewriteRule ^ - [L]
RewriteRule ^ /index.html
```
Opção - 2
É possivel adicionar o conteúdo acima citado em um arquivo `.htaccess` e adiciona-lo na pasta raiz do projeto no servidor.

Atenção! Em ambos os casos é necessário que o `rewrite_module` do Apache esteja ativo.

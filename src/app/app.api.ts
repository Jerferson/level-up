import { environment } from 'src/environments/environment';

export const JAMES_API = environment.james_api;
export const IBGE_API = environment.ibge_api;
export const BANK_API = environment.bank_api;

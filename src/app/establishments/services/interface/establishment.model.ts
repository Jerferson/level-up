export interface Establishment {
  id: string;
  name: string;
  picture: string;
  address: string;
  email: string;
  registered: string;
  phone: string;
  city: string;

  state: string;
  bank: string;
  acount_type: string;
  document: string;
  agency: string;
  agency_digit: number;
  account: string;
  account_digit: number;
  automatic: boolean;
}
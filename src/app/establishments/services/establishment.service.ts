import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { JAMES_API } from 'src/app/app.api';
import { Establishment } from './interface/establishment.model';

@Injectable({
  providedIn: 'root'
})
export class EstablishmentService {
  establishmentList: Establishment[];

  constructor(
    private httpClient: HttpClient
  ) {}

  /**
   * Function to get all Establishments of the API
   *
   * @return {*}  {Observable<Establishment[]>}
   * @memberof EstablishmentService
   */
  getAllEstablishments(): Observable<Establishment[]> {
    const localData = localStorage.getItem('establishments');
    if (localData) {
      this.establishmentList = JSON.parse(localData);

      return of(this.establishmentList);
    }

    return this.httpClient.get(`${JAMES_API}/establishments`).pipe(
      map(res => {
        this.establishmentList = res as Establishment[];

        return this.establishmentList;
      })
    );
  }

  save(establishment: Establishment): Observable<boolean> {
    let saved = false;
    this.establishmentList.forEach(item => {
      if (item.id === establishment.id) {
        item = establishment;
        saved = true;
      }
    });
    localStorage.setItem('establishments', JSON.stringify(this.establishmentList));

    return of(saved);
  }
}

import { Component, OnInit } from '@angular/core';
import { EstablishmentService } from '../services/establishment.service';
import { Establishment } from '../services/interface/establishment.model';

@Component({
  selector: 'app-establishment-list',
  templateUrl: './establishment-list.component.html',
  styleUrls: ['./establishment-list.component.css']
})
export class EstablishmentListComponent implements OnInit {
  establishmentList: Establishment[];

  constructor(
    private establishmentService: EstablishmentService
  ) { }

  ngOnInit(): void {
    this.listAllEstablishments();
  }

  /**
   * Function to get all Establishments
   */
  listAllEstablishments(): void {
    this.establishmentService.getAllEstablishments().subscribe(res => {
      this.establishmentList = res;
      this.establishmentList.sort((a, b) => a.name < b.name ? -1 : 1);
    });
  }

}

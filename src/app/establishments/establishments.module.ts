import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EstablishmentsRoutingModule } from './establishments-routing.module';
import { EstablishmentsComponent } from './establishments.component';
import { EstablishmentListComponent } from './establishment-list/establishment-list.component';
import { EstablishmentComponent } from './establishment/establishment.component';
import { EstablishmentDetailComponent } from './establishment-detail/establishment-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxMaskModule, IConfig } from 'ngx-mask';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;
@NgModule({
  declarations: [
    EstablishmentsComponent,
    EstablishmentListComponent,
    EstablishmentComponent,
    EstablishmentDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    FontAwesomeModule,
    EstablishmentsRoutingModule
  ]
})
export class EstablishmentsModule { }

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddressDefaultService } from 'src/app/shared/services/address-default.service';
import { BankDefaultService } from 'src/app/shared/services/bank-default.service';
import { Bank } from 'src/app/shared/services/interface/bank.model';
import { City } from 'src/app/shared/services/interface/city.model';
import { State } from 'src/app/shared/services/interface/state.model';
import { EstablishmentService } from '../services/establishment.service';
import { Establishment } from '../services/interface/establishment.model';
import { faLongArrowAltLeft  } from '@fortawesome/free-solid-svg-icons';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-establishment-detail',
  templateUrl: './establishment-detail.component.html',
  styleUrls: ['./establishment-detail.component.css']
})
export class EstablishmentDetailComponent implements OnInit {

  establishment: Establishment;
  establishmentList: Establishment[];
  stateList: State[];
  cityList: City[];
  bankList: Bank[];
  faLongArrowAltLeft  = faLongArrowAltLeft ;

  establishimentForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private establishmentService: EstablishmentService,
    private route: ActivatedRoute,
    private addressDefaultService: AddressDefaultService,
    private bankDefaultService: BankDefaultService
  ) { }

  ngOnInit(): void {
    this.getAllState();
    this.createEstablishmentForm();
    this.listenState();
    this.getEstablishment(this.route.snapshot.params.id);
    this.getAllBanks();
  }

  /**
   * Function listen change input State
   */
  listenState(): void {
    this.establishimentForm.controls['state'].valueChanges.subscribe(res => {
      this.getCityByState(res);
    });
  }

  /**
   * Function get the establishment by id
   * @param string
   */
  getEstablishment(id: string): void {
    this.establishmentService.getAllEstablishments().subscribe(res => {
      this.establishmentList = res;
      this.establishmentList.sort((a, b) => a.name < b.name ? -1 : 1);
      this.establishment = res.find(item => item.id === id);

      this.loadEstablishmentValues();
    });
  }

  /**
   * Function that loads establishment on the form
   */
  loadEstablishmentValues(): void {
    this.establishimentForm.controls['name'].setValue(this.establishment.name);
    this.establishimentForm.controls['address'].setValue(this.establishment.address);
    this.establishimentForm.controls['state'].setValue(this.establishment.state);
    this.establishimentForm.controls['city'].setValue(this.establishment.city);
    this.establishimentForm.controls['acount_type'].setValue(this.establishment.acount_type);
    this.establishimentForm.controls['document'].setValue(this.establishment.document);
    this.establishimentForm.controls['agency'].setValue(this.establishment.agency);
    this.establishimentForm.controls['agency_digit'].setValue(this.establishment.agency_digit);
    this.establishimentForm.controls['account'].setValue(this.establishment.account);
    this.establishimentForm.controls['account_digit'].setValue(this.establishment.account_digit);
    this.establishimentForm.controls['automatic'].setValue(this.establishment.automatic);
    this.establishimentForm.controls['bank'].setValue(this.establishment.bank);
  }

  /**
   * Function get all states and order by sigla
   */
  getAllState(): void {
    this.addressDefaultService.getAllState().subscribe(res => {
      this.stateList = res;
      this.stateList.sort((a, b) => a.sigla < b.sigla ? -1 : 1);
    });
  }

  /**
   * Function get all cities by state
   * @param uf
   */
  getCityByState(uf): void {
    this.addressDefaultService.getCityByState(uf).subscribe(res => {
      this.cityList = res;
      this.cityList.sort((a, b) => a.nome < b.nome ? -1 : 1);
    });
  }

  /**
   * Function start form
   */
  createEstablishmentForm(): void {
    this.establishimentForm = this.formBuilder.group({
      name: this.formBuilder.control(''),
      state: this.formBuilder.control(''),
      city: this.formBuilder.control(''),
      address: this.formBuilder.control(''),

      bank: this.formBuilder.control(''),
      acount_type: this.formBuilder.control(''),
      document: this.formBuilder.control(''),
      agency: this.formBuilder.control(''),
      agency_digit: this.formBuilder.control('', [Validators.maxLength(1)]),
      account: this.formBuilder.control(''),
      account_digit: this.formBuilder.control('', [Validators.maxLength(1)]),
      automatic: this.formBuilder.control('')
    });
  }

  /**
   * Function get all banks and order by name
   */
  getAllBanks(): void {
    this.bankDefaultService.getAllBanks().subscribe(res => {
      this.bankList = res;
      this.bankList.sort((a, b) => a.name < b.name ? -1 : 1);
    });
  }

  /**
   * Function save data update
   */
  save(): void {
    this.getEstablishmentEditedValues();
    this.establishmentService.save(this.establishment).subscribe(res => {
        if (res) {
          Swal.fire(
            'Salvo!',
            `Estabelecimento <b>${this.establishment.name}</b> alterado com sucesso!`,
            'success'
          );
          this.establishimentForm.reset();
          this.loadEstablishmentValues();
          return;
        }
        Swal.fire(
          'Algo deu errado!',
          `Desculpe! Não foi possível salvar a alteração para o estabelecimento <b>${this.establishment.name}</b>, tente novamente mais tarde!`,
          'error'
        );
    });
  }

  /**
   * Function updates data to save
   */
  getEstablishmentEditedValues(): void{
    this.establishment.name = this.establishimentForm.controls['name'].value;
    this.establishment.address = this.establishimentForm.controls['address'].value;
    this.establishment.state = this.establishimentForm.controls['state'].value;
    this.establishment.city = this.establishimentForm.controls['city'].value;
    this.establishment.acount_type = this.establishimentForm.controls['acount_type'].value;
    this.establishment.document = this.establishimentForm.controls['document'].value;
    this.establishment.agency = this.establishimentForm.controls['agency'].value;
    this.establishment.agency_digit = this.establishimentForm.controls['agency_digit'].value;
    this.establishment.account = this.establishimentForm.controls['account'].value;
    this.establishment.account_digit = this.establishimentForm.controls['account_digit'].value;
    this.establishment.automatic = this.establishimentForm.controls['automatic'].value;
    this.establishment.bank = this.establishimentForm.controls['bank'].value;
  }
}

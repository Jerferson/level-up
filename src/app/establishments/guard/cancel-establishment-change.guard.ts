import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { EstablishmentDetailComponent } from '../establishment-detail/establishment-detail.component';

import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class CancelEstablishmentChangeGuard implements CanDeactivate<EstablishmentDetailComponent> {
  canDeactivate(
    component: EstablishmentDetailComponent
  ): Promise<boolean | UrlTree> | boolean {
    if (!component.establishimentForm.dirty) {
      return true;
    }
    return Swal.fire({
      title: 'Atenção!',
      text: "Existem alterações que não foram salvas, deseja continuar mesmo assim?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sim, continuar!'
    }).then((result) => {
      if (result.isConfirmed) {
        return true;
      }
      return false;
    });
  }
}

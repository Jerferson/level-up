import { TestBed } from '@angular/core/testing';

import { CancelEstablishmentChangeGuard } from './cancel-establishment-change.guard';

describe('CancelEstablishmentChangeGuard', () => {
  let guard: CancelEstablishmentChangeGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CancelEstablishmentChangeGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

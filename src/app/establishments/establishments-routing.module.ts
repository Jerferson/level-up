import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EstablishmentDetailComponent } from './establishment-detail/establishment-detail.component';
import { EstablishmentListComponent } from './establishment-list/establishment-list.component';

import { EstablishmentsComponent } from './establishments.component';
import { CancelEstablishmentChangeGuard } from './guard/cancel-establishment-change.guard';

const routes: Routes = [
  { path: '', component: EstablishmentsComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: EstablishmentListComponent },
      {
        path: 'detail/:id',
        component: EstablishmentDetailComponent,
        canDeactivate: [CancelEstablishmentChangeGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstablishmentsRoutingModule { }

import { Injectable } from '@angular/core';
import { State } from './interface/state.model';
import { IBGE_API } from 'src/app/app.api';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { City } from './interface/city.model';

@Injectable({
  providedIn: 'root'
})
export class AddressDefaultService {

  constructor(
    private httpClient: HttpClient
  ) { }

  /**
   * Get all States
   *
   * @return {*}  {Observable<State[]>}
   * @memberof AddressDefaultService
   */
  getAllState(): Observable<State[]> {
    return this.httpClient.get(`${IBGE_API}/localidades/estados`).pipe(
      map(res => {
        return res as State[];
      })
    );
  }

  /**
   * Get all cities by state
   *
   * @param {string} uf
   * @return {*}  {Observable<City[]>}
   * @memberof AddressDefaultService
   */
  getCityByState(uf: string): Observable<City[]> {
    return this.httpClient.get(`${IBGE_API}/localidades/estados/${uf}/municipios`).pipe(
      map(res => {
        return res as State[];
      })
    );
  }
}

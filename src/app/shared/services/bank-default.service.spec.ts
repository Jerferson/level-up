import { TestBed } from '@angular/core/testing';

import { BankDefaultService } from './bank-default.service';

describe('BankDefaultService', () => {
  let service: BankDefaultService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BankDefaultService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BANK_API } from 'src/app/app.api';
import { Bank } from './interface/bank.model';

@Injectable({
  providedIn: 'root'
})
export class BankDefaultService {

  constructor(
    private httpClient: HttpClient
  ) { }

  /**
   * function get bank list default
   */
  getAllBanks(): Observable<Bank[]> {
    return this.httpClient.get(`${BANK_API}`).pipe(
      map(res => {
        return res as Bank[];
      })
    );
  }
}

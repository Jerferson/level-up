import { TestBed } from '@angular/core/testing';

import { AddressDefaultService } from './address-default.service';

describe('AddressDefaultService', () => {
  let service: AddressDefaultService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddressDefaultService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

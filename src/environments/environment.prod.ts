export const environment = {
  production: true,
  james_api: 'https://my-json-server.typicode.com/james-delivery/frontend-challenge',
  ibge_api: 'https://servicodados.ibge.gov.br/api/v1',
  bank_api: 'https://jerferson.github.io/bank-list/bancos.json'
};
